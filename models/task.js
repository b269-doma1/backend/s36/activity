// Contains what object are needed in our API

// Create the schema, models export te file

const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default : "pending"
	
	}

});

// "module.exports" is a way for Node JS to treat the value as a package that can be used by other files
module.exports = mongoose.model("Task", taskSchema);

