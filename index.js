// Setup the dependencies

const express = require("express");
const mongoose = require("mongoose");

// This allows us to use all the toutes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 3001;

// Middlewares
app.use(express.json());
app.use(express. urlencoded({extended: true}));


// Database connection
mongoose.connect("mongodb+srv://richrddoma:admin123@zuitt-bootcamp.xpydmcq.mongodb.net/s35?retryWrites=true&w=majority",
	{			
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

mongoose.connection.once("open", () => console.log(`Now connected to the cloud database `));

// Add task route
app.use("/task", taskRoute);


app.listen(port, () => console.log(`Now listening to port ${port}.`));


